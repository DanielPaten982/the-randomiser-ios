//
//  AboutView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 6/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit
import MessageUI

class AboutView: UIViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet var scrAbout: UIScrollView!
    @IBOutlet var lblVersion: UILabel!
    
    // App Version
    let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
    
    @IBAction func contactEmail() {
        // Check whether a mail account is set up on the device
        if !MFMailComposeViewController.canSendMail() {
            // Alert the user that they need to set up an account
            let alert = UIAlertController(title: "No Email Account", message: "You need to set up an account in the Mail app to send directly from here.", preferredStyle: .alert)
            
            let action = UIAlertAction(title: "OK", style: .cancel, handler: { (action: UIAlertAction) in
                print("User cannot send mail.")
            })
            
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        // Else, get the device details required for the email
        // Device
        var sysInfo = utsname()
        uname(&sysInfo) // ignore return value
        let device = String(bytes: Data(bytes: &sysInfo.machine, count: Int(_SYS_NAMELEN)), encoding: .ascii)!.trimmingCharacters(in: .controlCharacters)
        
        // iOS version
        let iOSversion = UIDevice.current.systemVersion
        
        let message = """
        What do you want to say?:
        
        
        Extra information for us, please don't delete!
        App Version: \(appVersion ?? "N/A")
        Device: \(device)
        iOS Version: \(iOSversion)
        """
        
        // Initialise the mail view controller
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.setToRecipients(["AussieAmateurs2016@gmail.com"])
        composeVC.setSubject("Support: The Randomiser (iOS)")
        composeVC.setMessageBody(message, isHTML: false)
        
        // Present the view controller modally.
        self.present(composeVC, animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        // Log to the system the outcome after presenting the controller.
        switch result {
        case .cancelled:
            print("Mail cancelled")
            break
        case .saved:
            print("Mail saved")
            break
        case .sent:
            print("Mail sent")
            break
        case .failed:
            print("Mail sent failure: \(error?.localizedDescription ?? "No error provided")")
            break
        default:
            break
        }
        
        // Dismiss the mail compose view controller.
        controller.dismiss(animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrAbout.isScrollEnabled = true
        lblVersion.text = "Version \(appVersion ?? "N/A")"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 1, green: 0.502, blue: 0, alpha: 1)
    }
}

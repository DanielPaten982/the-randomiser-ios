//
//  OrderView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class OrderView: UIViewController {
    
    @IBOutlet var txtIn: UITextView!
    @IBOutlet var txtOut: UITextView!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    
    @IBAction func order() {
        // Must be done regardless of outcome
        txtOut.isSelectable = false
        self.view.endEditing(true)
        
        // Check if the list is blank and notify the user if so
        if txtIn.text == "" || txtIn.text == " " {
            txtOut.text = "Nothing to order"
            return
        }
        
        // Collect the items
        let inputList = txtIn.text.components(separatedBy: "\n")
        var outputList = [String]()
        
        // Shuffle the list
        let amount = inputList.count
        
        if amount > 0 {
            outputList = inputList.shuffled()
        }
        
        txtOut.text = outputList.joined(separator: "\n")
    }
    
    @IBAction func clear() {
        // Remove all generated text
        txtIn.text = ""
        txtOut.text = ""
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        // Get the keyboard height and add that to the bottom constraint's size
        let keyboardSize = (notification.userInfo? [UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let keyboardHeight = keyboardSize?.height
        bottomConstraint.constant = keyboardHeight! - view.safeAreaInsets.bottom
        
        bottomConstraint.priority = UILayoutPriority(rawValue: 999)
        view.layoutIfNeeded()
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        // Reset the constraints to their original properties
        bottomConstraint.constant = 12
        bottomConstraint.priority = UILayoutPriority(rawValue: 250)
        view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Beautify interface
        txtIn.layer.cornerRadius = 5
        txtIn.layer.borderWidth = 1
        txtIn.layer.borderColor = UIColor.systemGray.cgColor
        txtOut.layer.cornerRadius = 5
        txtIn.autocorrectionType = .no
        
        // Add observers for the keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0.588, green: 0.239, blue: 0.592, alpha: 1)
    }
}

//
//  DiceHelpView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class DiceHelpView: UIViewController {
    
    @IBOutlet var scrDiceHelp: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrDiceHelp.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0.878, green: 0.227, blue: 0.243, alpha: 1)
    }
}

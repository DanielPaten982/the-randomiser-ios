//
//  ItemView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class ItemView: UIViewController {
    
    @IBOutlet var lblItemSel: UILabel!
    @IBOutlet var lblStatic: UILabel!
    @IBOutlet var txtIn: UITextView!
    @IBOutlet var bottomItem: NSLayoutConstraint!
    
    @IBAction func pickItem() {
        // Must be done regardless of outcome
        self.view.endEditing(true)
        
        // Check if the list is blank and notify the user if so
        if txtIn.text == "" || txtIn.text == " " {
            lblItemSel.text = "Nothing to select"
            return
        }
        
        // Collect the items
        let inputList = txtIn.text.components(separatedBy: "\n")
        
        let amount = inputList.count
        
        // Select a random item and write to output
        if amount > 0 {
            lblItemSel.text = inputList.randomElement()
        }
    }
    
    @IBAction func clear() {
        // Remove all generated text and reset colouring
        txtIn.text = ""
        lblItemSel.text = ""
        self.view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        // Get the keyboard height and add that to the bottom constraint's size
        let keyboardSize = (notification.userInfo? [UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
        let keyboardHeight = keyboardSize?.height
        bottomItem.constant = keyboardHeight! - view.safeAreaInsets.bottom
        
        bottomItem.priority = UILayoutPriority(rawValue: 999)
        view.layoutIfNeeded()
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        // Reset the constraints to their original properties
        bottomItem.constant = 12
        bottomItem.priority = UILayoutPriority(rawValue: 250)
        view.layoutIfNeeded()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Beautify interface
        txtIn.layer.cornerRadius = 5
        txtIn.layer.borderWidth = 1
        txtIn.layer.borderColor = UIColor.systemGray.cgColor
        lblItemSel.layer.cornerRadius = 5
        lblItemSel.layer.masksToBounds = true
        txtIn.autocorrectionType = .no
        
        // Add observers for the keyboard
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0, green: 0.616, blue: 0.863, alpha: 1)
    }
}

//
//  NumberView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class NumberView: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var txtNumMin: UITextField!
    @IBOutlet var txtNumMax: UITextField!
    @IBOutlet var lblNumSel: UILabel!
    @IBOutlet var lblStatic: UILabel!
    
    @IBAction func pickNumber() {
        // Get int values from text fields
        var min = Int(txtNumMin.text ?? "") ?? 0
        var max = Int(txtNumMax.text ?? "") ?? 0
        
        // Check the minimum is lower than the maximum
        if min > max {
            // Swap the values in the text fields
            let tempMin = txtNumMin.text
            txtNumMin.text = txtNumMax.text
            txtNumMax.text = tempMin
            
            // Swap the integer values
            let tempInt = min
            min = max
            max = tempInt
        }
        
        // Pick and display a number within the specified range
        let number = Int.random(in: min ... max)
        lblNumSel.text = "\(number)"
        
        self.view.endEditing(true)
    }
    
    @IBAction func clear() {
        // Remove all generated text and reset colouring
        txtNumMin.text = ""
        txtNumMax.text = ""
        lblNumSel.text = ""
        self.view.endEditing(true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Beautify interface
        lblNumSel.layer.cornerRadius = 5
        lblNumSel.layer.masksToBounds = true
        txtNumMin.autocorrectionType = .no
        txtNumMax.autocorrectionType = .no
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0.38, green: 0.733, blue: 0.275, alpha: 1)
    }
}

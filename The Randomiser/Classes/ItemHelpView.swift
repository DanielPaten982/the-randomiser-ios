//
//  ItemHelpView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class ItemHelpView: UIViewController {
    
    @IBOutlet var scrItemHelp: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrItemHelp.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0, green: 0.616, blue: 0.863, alpha: 1)
    }
}

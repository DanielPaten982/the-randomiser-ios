//
//  DiceTimer.swift
//  The Randomiser
//
//  Created by Daniel Paten on 8/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

let notify = "diceKey"

class DiceTimer {
    private var animationNum = 0
    private var diceTimer: Timer?
    
    init(diceNum: Int) {
        diceTimer = Timer.scheduledTimer(withTimeInterval: 0.09, repeats: true) { diceTimer in
            // Change dice animation upon every fire
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: notify), object: nil, userInfo: ["dn": diceNum, "an": self.animationNum])
            self.animationNum += 1
            if self.animationNum == 9 { self.animationNum = 0 } // loop animation index
        }
    }
    
    func invalidateTimer() {
        diceTimer?.invalidate()
    }
    
    func isRunning() -> Bool {
        return diceTimer!.isValid
    }
}

//
//  DiceView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class DiceView: UIViewController {
    
    // Initialise the image arrays
    let diceImg = [UIImage(named: "Dice1")!, UIImage(named: "Dice2")!, UIImage(named: "Dice3")!, UIImage(named: "Dice4")!, UIImage(named: "Dice5")!, UIImage(named: "Dice6")!]
    let diceAnim = [UIImage(named: "DiceAnime0")!, UIImage(named: "DiceAnime1")!, UIImage(named: "DiceAnime2")!, UIImage(named: "DiceAnime3")!, UIImage(named: "DiceAnime4")!, UIImage(named: "DiceAnime5")!, UIImage(named: "DiceAnime6")!, UIImage(named: "DiceAnime7")!, UIImage(named: "DiceAnime8")!]
    
    // Initialise timer classes
    var tmrOne: DiceTimer?
    var tmrTwo: DiceTimer?
    var tmrThree: DiceTimer?
    var tmrFour: DiceTimer?
    var tmrFive: DiceTimer?
    var tmrSix: DiceTimer?
    
    @IBOutlet var btnOne: UIButton!
    @IBOutlet var btnTwo: UIButton!
    @IBOutlet var btnThree: UIButton!
    @IBOutlet var btnFour: UIButton!
    @IBOutlet var btnFive: UIButton!
    @IBOutlet var btnSix: UIButton!
    @IBOutlet var lblDiceNum: UILabel!
    @IBOutlet var sliDice: UISlider!
    
    @IBAction func diceOne() {
        // Check whether the timer is active to determine whether to start or stop rolling the dice
        if tmrOne != nil && (tmrOne?.isRunning())! {
            tmrOne?.invalidateTimer()
            btnOne.setBackgroundImage(diceImg.randomElement(), for: .normal)
        } else {
            // Create the timer with its properties
            tmrOne = DiceTimer(diceNum: 1)
        }
    }
    
    @IBAction func diceTwo() {
        // Check whether the timer is active to determine whether to start or stop rolling the dice
        if tmrTwo != nil && (tmrTwo?.isRunning())! {
            tmrTwo?.invalidateTimer()
            btnTwo.setBackgroundImage(diceImg.randomElement(), for: .normal)
        } else {
            // Create the timer with its properties
            tmrTwo = DiceTimer(diceNum: 2)
        }
    }
    
    @IBAction func diceThree() {
        // Check whether the timer is active to determine whether to start or stop rolling the dice
        if tmrThree != nil && (tmrThree?.isRunning())! {
            tmrThree?.invalidateTimer()
            btnThree.setBackgroundImage(diceImg.randomElement(), for: .normal)
        } else {
            // Create the timer with its properties
            tmrThree = DiceTimer(diceNum: 3)
        }
    }
    
    @IBAction func diceFour() {
        // Check whether the timer is active to determine whether to start or stop rolling the dice
        if tmrFour != nil && (tmrFour?.isRunning())! {
            tmrFour?.invalidateTimer()
            btnFour.setBackgroundImage(diceImg.randomElement(), for: .normal)
        } else {
            // Create the timer with its properties
            tmrFour = DiceTimer(diceNum: 4)
        }
    }
    
    @IBAction func diceFive() {
        // Check whether the timer is active to determine whether to start or stop rolling the dice
        if tmrFive != nil && (tmrFive?.isRunning())! {
            tmrFive?.invalidateTimer()
            btnFive.setBackgroundImage(diceImg.randomElement(), for: .normal)
        } else {
            // Create the timer with its properties
            tmrFive = DiceTimer(diceNum: 5)
        }
    }
    
    @IBAction func diceSix() {
        // Check whether the timer is active to determine whether to start or stop rolling the dice
        if tmrSix != nil && (tmrSix?.isRunning())! {
            tmrSix?.invalidateTimer()
            btnSix.setBackgroundImage(diceImg.randomElement(), for: .normal)
        } else {
            // Create the timer with its properties
            tmrSix = DiceTimer(diceNum: 6)
        }
    }
    
    @IBAction func rollAll() {
        // Invalidate all currently active timers
        invalidateAll()
        
        // Initialise the timer for each dice
        tmrOne = DiceTimer(diceNum: 1)
        tmrTwo = DiceTimer(diceNum: 2)
        tmrThree = DiceTimer(diceNum: 3)
        tmrFour = DiceTimer(diceNum: 4)
        tmrFive = DiceTimer(diceNum: 5)
        tmrSix = DiceTimer(diceNum: 6)
    }
    
    @IBAction func stopAll() {
        // Invalidate all currently active timers
        invalidateAll()
        
        // Select a dice image for each dice
        btnOne.setBackgroundImage(diceImg.randomElement(), for: .normal)
        btnTwo.setBackgroundImage(diceImg.randomElement(), for: .normal)
        btnThree.setBackgroundImage(diceImg.randomElement(), for: .normal)
        btnFour.setBackgroundImage(diceImg.randomElement(), for: .normal)
        btnFive.setBackgroundImage(diceImg.randomElement(), for: .normal)
        btnSix.setBackgroundImage(diceImg.randomElement(), for: .normal)
    }
    
    @IBAction func changeNumber() {
        // Change the number displayed in the label based on the current value of the slider
        let slider = Int(sliDice.value)
        lblDiceNum.text = "\(slider)"
        
        // Show/hide the dice images & enable/disable the buttons based on the value of the stepper
        switch slider {
        case 1:
            btnOne.isHidden = false
            btnTwo.isHidden = true
            btnThree.isHidden = true
            btnFour.isHidden = true
            btnFive.isHidden = true
            btnSix.isHidden = true
            break
        case 2:
            btnOne.isHidden = false
            btnTwo.isHidden = false
            btnThree.isHidden = true
            btnFour.isHidden = true
            btnFive.isHidden = true
            btnSix.isHidden = true
            break
        case 3:
            btnOne.isHidden = false
            btnTwo.isHidden = false
            btnThree.isHidden = false
            btnFour.isHidden = true
            btnFive.isHidden = true
            btnSix.isHidden = true
            break
        case 4:
            btnOne.isHidden = false
            btnTwo.isHidden = false
            btnThree.isHidden = false
            btnFour.isHidden = false
            btnFive.isHidden = true
            btnSix.isHidden = true
            break
        case 5:
            btnOne.isHidden = false
            btnTwo.isHidden = false
            btnThree.isHidden = false
            btnFour.isHidden = false
            btnFive.isHidden = false
            btnSix.isHidden = true
            break
        case 6:
            btnOne.isHidden = false
            btnTwo.isHidden = false
            btnThree.isHidden = false
            btnFour.isHidden = false
            btnFive.isHidden = false
            btnSix.isHidden = false
            break
        default:
            break
        }
    }
    
    func invalidateAll() {
        // Invalidate all currently active timers
        tmrOne?.invalidateTimer()
        tmrTwo?.invalidateTimer()
        tmrThree?.invalidateTimer()
        tmrFour?.invalidateTimer()
        tmrFive?.invalidateTimer()
        tmrSix?.invalidateTimer()
    }
    
    func changeAnimation(ntn: Notification) {
        // Process dice timer class data
        guard let diceNum: Int = ntn.userInfo!["dn"] as? Int else { return }
        guard let aniNum: Int = ntn.userInfo!["an"] as? Int else { return }
        
        // Set animation background
        switch diceNum {
        case 1:
            btnOne.setBackgroundImage(diceAnim[aniNum], for: .normal)
            break
        case 2:
            btnTwo.setBackgroundImage(diceAnim[aniNum], for: .normal)
            break
        case 3:
            btnThree.setBackgroundImage(diceAnim[aniNum], for: .normal)
            break
        case 4:
            btnFour.setBackgroundImage(diceAnim[aniNum], for: .normal)
            break
        case 5:
            btnFive.setBackgroundImage(diceAnim[aniNum], for: .normal)
            break
        case 6:
            btnSix.setBackgroundImage(diceAnim[aniNum], for: .normal)
            break
        default:
            break
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Notification to receive dice timer class data
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: notify),
        object: nil,
        queue: nil,
        using: changeAnimation)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0.878, green: 0.227, blue: 0.243, alpha: 1)
    }
}

//
//  NumberHelpView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class NumberHelpView: UIViewController {
    
    @IBOutlet var scrNumberHelp: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrNumberHelp.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0.38, green: 0.733, blue: 0.275, alpha: 1)
    }
}

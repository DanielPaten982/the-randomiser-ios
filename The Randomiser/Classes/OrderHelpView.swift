//
//  OrderHelpView.swift
//  The Randomiser
//
//  Created by Daniel Paten on 7/3/20.
//  Copyright © 2020 D3PO. All rights reserved.
//

import UIKit

class OrderHelpView: UIViewController {
    
    @IBOutlet var scrOrderHelp: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scrOrderHelp.isScrollEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.tintColor = UIColor(displayP3Red: 0.588, green: 0.239, blue: 0.592, alpha: 1)
    }
}
